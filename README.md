# Redis Queue Listener

Simple example creating queue lisetner with node and redis

## Prerequisites

Please make sure you had install and running redis on your machine

## Installation

If you don't had yarn package manager please install [yarn](https://classic.yarnpkg.com/en/docs/install) 

And run the command below

```bash
yarn install
```

## Build and start server

Please run the below command step by step in the root of the project

```bash
yarn start:dev
```

## Running the example

Please clone and start the server [redis-queue-server](https://bitbucket.org/Eidolex/redis-queue-server/src)


