import Queue from 'bull';
import Redis from 'ioredis';
const client = new Redis();
const subscriber = new Redis();

const opts = {
    createClient: function (type: string) {
      switch (type) {
        case 'client':
            return client;
        case 'subscriber':
          return subscriber;
        default:
          return new Redis();
      }
    }
  }

const fcmQueue = new Queue("fcm queue", opts);

fcmQueue.process((job, done) => {
    console.log(job.id);
    console.log(new Date().toString());
    setTimeout(() => {
        done();
    }, 3000)
})


fcmQueue.on('completed', (job) => {
    const { id, data: { uniqueKey, appKey } } = job;
    client.publish(appKey, JSON.stringify({
        uniqueKey,
        jobId: id
    }));
})
